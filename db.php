<?php

$host = "mysql"; // assuming this is the hostname of your MySQL container
$port = "3306"; // assuming this is the port MySQL is listening on
$database = "mydatabase"; // replace with your actual database name
$username = "myuser"; // replace with your actual username
$password = "mypassword"; // replace with your actual password

// Attempt to connect to MySQL
$dsn = "mysql:host=$host;port=$port;dbname=$database;charset=utf8";
try {
    $pdo = new PDO($dsn, $username, $password);
    echo "Connected to MySQL successfully!\n";
    
    // Execute a test query
    $stmt = $pdo->query("SELECT * FROM your_table LIMIT 1");
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($result) {
        echo "Test query executed successfully!\n";
    } else {
        echo "Test query failed!\n";
    }
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage() . "\n";
}

