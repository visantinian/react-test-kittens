// craco.config.js
module.exports = {
	webpack: {
		configure: (webpackConfig, { env, paths }) => {
			// Remove hash from output filename
			webpackConfig.output.filename = 'static/js/[name].js';
			webpackConfig.output.chunkFilename = 'static/js/[name].chunk.js';

			return webpackConfig;
		},
	},
};
