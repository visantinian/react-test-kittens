import React, { useState, useEffect } from 'react';
import axios from 'axios';
import AddKittenModal from './AddKittenModal';
import KittenList from './KittenList';

const ProfilePage = () => {
	const [showKittenModal, setShowKittenModal] = useState(false);
	const [kittens, setKittens] = useState([]);

	useEffect(() => {
		fetchKittens();
	}, []);

	const fetchKittens = () => {
		axios.get('/api/get-initial-data')
			.then(response => {
				setKittens(response.data.kittens);
			})
			.catch(error => console.error('Error fetching kittens:', error));
	};

	const handleOpenKittenModal = () => {
		setShowKittenModal(true);
	};

	const handleCloseModal = () => {
		setShowKittenModal(false);
	};

	const onAddKitten = () => {
		fetchKittens();
	};

	const handleAddKitten = (newKitten) => {
		setKittens([...kittens, newKitten]);
	};

	return (
		<div>
			<h1 className="main-h1">Вітаємо в особистому кабінеті</h1>
			<p className="your-kittens">Ваші котики</p>
			<KittenList kittens={kittens} />
			{showKittenModal && <AddKittenModal onClose={handleCloseModal} onAddKitten={handleAddKitten} />}
			<div className="add-kitten-wrapper">
				<div className="add-kitten-info-wrapper">
					<p className="add-kitten-text">Додати новго котика та <br/>зареєструвати на курс</p>
					<button className="add-kitten-btn" onClick={handleOpenKittenModal}>Додати котика</button>
				</div>
			</div>

		</div>
	);
};

export default ProfilePage;
