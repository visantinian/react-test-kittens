import React from 'react';

const CoursesList = ({ allCourses, selectedCoursesIds, onCourseToggle }) => {
	const isCourseSelected = (courseId) => {
		return selectedCoursesIds.includes(courseId);
	};
	const handleToggle = (courseId) => {
		onCourseToggle(courseId);
	};
	return (
		<div className="courses-list">
			{allCourses.map((course) => (
				<label key={course.id} className="course-item">
					<input
						type="checkbox"
						checked={isCourseSelected(course.id)}
						onChange={() => handleToggle(course.id)}
					/>
					<p className="course-item-label"> {course.name} - ₴{parseInt(course.price, 10)} </p>

				</label>
			))}
		</div>
	);
};

export default CoursesList;
