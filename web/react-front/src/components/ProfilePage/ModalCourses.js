import React, { useState, useEffect } from 'react';

const ModalCourses = ({ isOpen, onClose, courses, selectedCourses, onUpdateCourses, kittenId }) => {
	const [selectedCourseIds, setSelectedCourseIds] = useState(new Set());

	// Update selectedCourseIds whenever selectedCourses changes
	useEffect(() => {
		setSelectedCourseIds(new Set(selectedCourses.map(course => course.id)));
	}, [selectedCourses]);

	const handleCourseToggle = (courseId) => {
		const id = courseId.toString();
		setSelectedCourseIds(prevSelectedCourseIds => {
			const newSelectedCourseIds = new Set(prevSelectedCourseIds);
			if (newSelectedCourseIds.has(id)) {
				newSelectedCourseIds.delete(id);
			} else {
				newSelectedCourseIds.add(id);
			}
			return newSelectedCourseIds;
		});
	};


	const handleSubmit = () => {
		const updatedCourseIds = Array.from(selectedCourseIds);
		onUpdateCourses(kittenId, updatedCourseIds);
		onClose();
	};

	if (!isOpen) return null;

	return (
		<div className="modal-backdrop">
			<div className="modalCourses">
				<div className="modal-content">
					<h2>Edit Courses</h2>
					{courses.map(course => (
						<label key={course.id}>
							<input
								type="checkbox"
								checked={selectedCourseIds.has(course.id.toString())}
								onChange={() => handleCourseToggle(course.id)}
							/>
							{course.name} - ₴{parseInt(course.price, 10)}
						</label>
					))}
					<button onClick={handleSubmit}>Update Courses</button>
					<button onClick={onClose}>Cancel</button>
				</div>
			</div>
		</div>
	);
};

export default ModalCourses;
