import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CoursesList from './CoursesList';


const AddKittenModal = ({ onClose, onKittenAdded }) => {
	const [courses, setCourses] = useState([]);
	const [selectedCourses, setSelectedCourses] = useState('');
	const [kittenName, setKittenName] = useState('');
	const [kittenLastName, setKittenLastName] = useState('');
	const [kittenAge, setKittenAge] = useState('');
	const [kittenMobile, setKittenMobile] = useState('');
	const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');


	const onBackdropClick = (event) => {
		onClose();
	};
	const onModalClick = (event) => {
		event.stopPropagation();
	};
	useEffect(() => {
		const handleEscape = (event) => {
			if (event.key === 'Escape') {
				onClose();
			}
		};
		window.addEventListener('keydown', handleEscape);
		return () => {
			window.removeEventListener('keydown', handleEscape);
		};
	}, [onClose]);


	useEffect(() => {
		axios.get('/api/get-initial-data')
			.then(response => {
				setCourses(response.data.courses);
			})
			.catch(error => {
				console.error('Error fetching courses:', error);
			});
	}, []);

	const handleSubmit = (event) => {
		event.preventDefault();
		axios.post('/api/add-kitten', {
			name: kittenName,
			last_name: kittenLastName,
			age: kittenAge,
			phone_number: kittenMobile,
			course_ids: selectedCourses
		})
			.then(response => {
				if (response.data.success) {
					alert('Kitten added!');
					onClose();
				} else {
					alert('Failed to add kitten: ' + JSON.stringify(response.data.error));
				}
			})
			.catch(error => {
				console.error('Error adding kitten:', error);
				alert('Error adding kitten');
			});
		onClose();
	};

	const handleCourseToggle = (courseId) => {
		setSelectedCourses(prevCourses =>
			prevCourses.includes(courseId)
				? prevCourses.filter(id => id !== courseId) // Remove if already selected
				: [...prevCourses, courseId] // Add if not already selected
		);
	};

	return (
		<div className="modal-backdrop" onClick={onBackdropClick}>
			<div className="modal-kitten-form" onClick={onModalClick}>
				<div className="modal-content">
					<span className="close" onClick={onClose}>&times;</span>
					<p className="add-kitten-modal-title">Додати нового котика та зареєструвати на курс</p>
					<p className="add-kitten-modal-subtitle">Для реєстрації котика на курс заповніть, будь ласка, форму нижче.
						Всі поля обов’язкові для заповнення.</p>

					<p className="add-kitten-modal-form-title">Дані котика.</p>
					<form className="add-kitten-modal-form" onSubmit={handleSubmit}>
						<div className="inputs">
							<input type="text" id="lastName" placeholder="Прізвище" value={kittenLastName} onChange={e => setKittenLastName(e.target.value)} />
							<input type="text" id="name" placeholder="Ім'я" value={kittenName} onChange={e => setKittenName(e.target.value)} />
							<input type="text" id="age" placeholder="Вік" value={kittenAge} onChange={e => setKittenAge(e.target.value)} />
							<input type="text" id="mobile" placeholder="Номер котофону" value={kittenMobile} onChange={e => setKittenMobile(e.target.value)} />
						</div>

						<label htmlFor="course">Оберіть курси:</label>
						<CoursesList
							allCourses={courses}
							selectedCoursesIds={selectedCourses}
							onCourseToggle={handleCourseToggle}
						/>
						<button type="submit">Add Kitten</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default AddKittenModal;
