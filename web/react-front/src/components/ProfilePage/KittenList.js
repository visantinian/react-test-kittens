import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ModalCourses from './ModalCourses';

const KittenList = () => {
    const [kittens, setKittens] = useState([]);
    const [courses, setCourses] = useState([]); // This will store all possible courses
    const [isModalCoursesOpen, setIsModalCoursesOpen] = useState(false);
    const [currentEditingKitten, setCurrentEditingKitten] = useState(null);
    const csrfToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    axios.defaults.headers.common['X-CSRF-Token'] = csrfToken;


    const fetchData = () => {
        axios.get('/api/get-initial-data')
            .then(response => {
                setKittens(response.data.kittens);
                setCourses(response.data.courses);
            })
            .catch(error => {
                console.error('Error fetching data:', error);
            });
    };


    useEffect(() => {
        fetchData();
    }, []);

    const handleOpenModal = (kitten) => {
        setCurrentEditingKitten(kitten);
        setIsModalCoursesOpen(true);
    };


    const handleCloseModal = () => {
        setIsModalCoursesOpen(false);
    };

    const updateKittenCourses = (kittenId, updatedCourseIds) => {
        axios.post('/api/assign-courses', {
            kittenId: kittenId,
            courseIds: Array.from(updatedCourseIds)
        }, {
            headers: {
                'Content-Type': 'application/json',
                'X-CSRF-Token': csrfToken
            }
        })
            .then(response => {
                if (response.data.success) {
                    console.log('Courses updated:', response.data);
                    fetchData();  // Refetch data to update UI
                }
            })
            .catch(error => {
                console.error('Error updating courses:', error);
            });
    };

    return (
        <div className="wrapper-kittens-list">
            {kittens.map(kitten => (
                <div className="kitten-profile" key={kitten?.id}>
                    <div className="kitten-left-side-wrapper">
                        <img src={kitten.image_url || 'default-image.jpg'} alt={kitten?.name} className="kitten-avatar" />
                        <div className="kitten-info-wrapper">
                            <div className="kitten-info">
                                <p className="kitten-name">{kitten?.name} {kitten.last_name}</p>
                                <p className="kitten-age">{kitten?.age}</p>
                                <p className="kitten-phone">{kitten?.phone_number}</p>
                            </div>
                        </div>
                    </div>
                    <div className="right-side-wrapper">
                        <div className="courses-wrapper">
                            <span className="selected-courses-title"> Обрані курси:</span>
                            <div className="selected-courses">
                                {kitten?.courses?.map(course => (
                                    <div className="course-item" key={course?.id}>
                                        {course?.name} - ₴{course?.price}
                                    </div>
                                ))}
                            </div>
                            <button className="add-course-button" onClick={() => handleOpenModal(kitten)}>Додати новий курс</button>
                        </div>
                    </div>
                </div>
            ))}
            {isModalCoursesOpen && currentEditingKitten && (
                <ModalCourses
                    isOpen={isModalCoursesOpen}
                    onClose={handleCloseModal}
                    courses={courses}
                    selectedCourses={currentEditingKitten?.courses}
                    onUpdateCourses={updateKittenCourses}
                    kittenId={currentEditingKitten?.id}
                />
            )}
        </div>
    );
};

export default KittenList;
