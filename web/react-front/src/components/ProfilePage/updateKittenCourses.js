const updateKittenCourses = (kittenId, updatedCourseIds) => {
	const data = {
		kittenId: kittenId,
		courseIds: updatedCourseIds
	};

	axios.post('/api/assign-courses', data)
		.then(response => {
			if (response.data.success) {
				console.log(response.data.message);
			}
		})
		.catch(error => {
			console.error('Error updating courses:', error);
		});
};
