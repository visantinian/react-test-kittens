<?php
use yii\helpers\Html;
use app\assets\AppAsset;
use app\widgets\Alert;

// Register the AppAsset
AppAsset::register($this);

// Begin the page
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>

	<!-- Load React dependencies -->
	<script src="https://unpkg.com/react@18.2.0/umd/react.development.js"></script>
	<script src="https://unpkg.com/react-dom@18.2.0/umd/react-dom.development.js"></script>

	<!-- Include the bundle.js file -->
	<script src="<?= Yii::$app->request->baseUrl ?>/react-front/dist/bundle.js"></script>
	<?php echo \yii\helpers\Html::csrfMetaTags(); ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
	<!-- Include header partial -->
	<?= $this->render('/layouts/_header') ?>
</header>

<main id="react-main" class="flex-shrink-0" role="main">
	<div class="react-container">
		<?= Alert::widget() ?>

		<!-- React container -->
		<div id="profile-container"></div>
	</div>
</main>

<footer id="footer" class="mt-auto py-3 bg-light">
	<div class="container">
		<div class="row text-muted">
			<div class="col-md-6 text-center text-md-start">&copy; My Company <?= date('Y') ?></div>
			<div class="col-md-6 text-center text-md-end"><?= Yii::powered() ?></div>
		</div>
	</div>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
