<?php

use yii\helpers\Html;
use yii\bootstrap5\NavBar;
use yii\bootstrap5\Nav;

NavBar::begin([
  'brandLabel' => Html::img('@web/logo.png', ['alt'=>Yii::$app->name]),
  'brandUrl' => Yii::$app->homeUrl,
  'options' => [
    'class' => 'navbar-expand-md fixed-top navbar-light bg-light',
  ],
]);
echo Nav::widget([
  'options' => ['class' => 'navbar-nav mr-auto'],
  'items' => [
    ['label' => 'Item 1', 'url' => ['/site/my-kittens-list']],
    ['label' => 'Item 2', 'url' => ['/site/my-kittens-list']],
    ['label' => 'Item 3', 'url' => ['/site/my-kittens-list']],
    ['label' => 'Item 4', 'url' => ['/site/my-kittens-list']],
    ['label' => 'Item 5', 'url' => ['/site/my-kittens-list']],
  ],
]);
echo '<div class="nav-item dropdown">';
echo Html::a('Language', '#', ['class' => 'nav-link dropdown-toggle', 'data-toggle' => 'dropdown']);
print_r(
  '<div class="dropdown-menu">
                <a class="dropdown-item" href="#">УКР</a>
                <a class="dropdown-item" href="#">ENG</a>
             </div>
          </div>
        <button class="btn btn-primary ml-2">Button 1</button>
        <button class="btn btn-secondary ml-2">Button 2</button>
        <a href="/site/my-kittens-list">
        <img src="avatar.jpg" class="ml-2 rounded-circle" width="32" height="32" alt="Avatar">
        </a>'
);

NavBar::end();
?>