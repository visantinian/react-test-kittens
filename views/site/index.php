<?php

/** @var yii\web\View $this */
use yii\web\View;
$this->title = 'My Yii Application';
$this->registerCssFile('@web/css/style.css', ['position' => View::POS_HEAD]);
?>

<main>
    <h1> Онлайн курси котячої творчості </h1>
    <p>Навчимо вашого котика основам створення цікавого життя, сповненого веселих та неередбачуваних моментів для вас та ваших рідних.</p>
    <div class="product-grid">
        <div class="product">
            <img src="https://placehold.it/280x130" alt="Бісквіт">
            <span class="yellow-text">~280</span>
            <span class="normal-text">Виготовлених дизайнерських крісел</span>
        </div>
        <div class="product">
            <img src="https://placehold.it/280x130" alt="Провезник">
            <span class="yellow-text">~1300</span>
            <span class="normal-text">Проведених садівничих робіт</span>
        </div>
        <div class="product">
            <img src="https://placehold.it/280x130" alt="Батон">
            <span class="yellow-text">~340</span>
            <span class="normal-text">Влаштованих нічних концертів</span>
        </div>
        <div class="product">
            <img src="https://placehold.it/280x130" alt="Ниночник">
            <span class="yellow-text">~90000</span>
            <span class="normal-text">Виконаних невтомних тіпаньків</span>
        </div>
    </div>
</main>
