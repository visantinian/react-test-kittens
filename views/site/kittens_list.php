<?php
/** @var yii\web\View $this */
use yii\web\View;

$this->title = 'My Yii Application';
$this->registerCssFile('@web/css/style.css', ['position' => View::POS_HEAD]);
