<?php
/** @var yii\web\View $this */
use yii\web\View;

$this->title = 'My Yii Application';
$this->registerCssFile('@web/css/style.css', ['position' => View::POS_HEAD]);


// Optionally, you can pass data from your Yii2 controller to your React app
$initialData = json_encode([
    // Define your initial data here
]);

