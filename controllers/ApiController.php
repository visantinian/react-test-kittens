<?php
namespace app\controllers;

use Yii;
use yii\rest\Controller;
use app\models\Courses;
use app\models\Kittens;
use app\models\KittenCourse;
use yii\helpers\Json;

class ApiController extends Controller
{
  public function actionGetInitialData()
  {
    $data = [
      'courses' => Courses::find()->all(),
      'kittens' => Kittens::find()->with('courses')->asArray()->all(),
    ];
    $response = \Yii::$app->response;
    $response->format = \yii\web\Response::FORMAT_JSON;
    $response->data = $data;
    return $response;
  }

  public function actionAssignCourses()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $postData = \Yii::$app->request->post();
    if (!isset($postData['kittenId']) || !isset($postData['courseIds'])) {
      return ['success' => false, 'error' => 'Required parameters missing'];
    }

    $kittenId = $postData['kittenId'];
    $courseIds = $postData['courseIds'];
    $transaction = \Yii::$app->db->beginTransaction();
    try {
      KittenCourse::deleteAll(['kitten_id' => $kittenId]);

      foreach ($courseIds as $courseId) {
        $kittenCourse = new KittenCourse();
        $kittenCourse->kitten_id = $kittenId;
        $kittenCourse->course_id = $courseId;
        if (!$kittenCourse->save()) {
          throw new \yii\db\Exception('Failed to save KittenCourse: ' . Json::encode($kittenCourse->errors));
        }
      }

      $transaction->commit();
      return ['success' => true, 'message' => 'Courses have been assigned.'];
    } catch (\Exception $e) {
      $transaction->rollBack();
      \Yii::error("Failed to assign courses: " . $e->getMessage(), __METHOD__);
      return ['success' => false, 'error' => $e->getMessage()];
    }
  }
  public function actionAddKitten() {
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $request = Yii::$app->request;

    if ($request->isPost) {
      $kittenData = $request->post();
      $kitten = new Kittens();
      $kitten->attributes = $kittenData;

      if ($kitten->save()) {
        return [
          'success' => true,
          'kitten' => $kitten,
          'message' => 'Kitten added successfully.'
        ];
      } else {
        return [
          'success' => false,
          'errors' => $kitten->errors
        ];
      }
    } else {
      return [
        'success' => false,
        'error' => 'Invalid request method.'
      ];
    }
  }

}
