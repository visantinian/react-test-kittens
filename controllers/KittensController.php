<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Kittens;

class KittensController extends ActiveController
{
  public $modelClass = 'app\models\Kittens';

  public function actionIndex()
  {
    return Kittens::find()->all();
  }

  public function actionView($id)
  {
    return Kittens::findOne($id);
  }

  public function actionCreate()
  {
    $model = new Kittens();
    $model->load(Yii::$app->getRequest()->getBodyParams(), '');
    if ($model->save()) {
      return $model;
    } else {
      return $model->errors;
    }
  }

  public function actionUpdate($id)
  {
    $model = Kittens::findOne($id);
    if ($model === null) {
      throw new \yii\web\NotFoundHttpException("Kitten not found with ID: $id");
    }

    $model->load(Yii::$app->getRequest()->getBodyParams(), '');
    if ($model->save()) {
      return $model;
    } else {
      return $model->errors;
    }
  }

  public function actionDelete($id)
  {
    $model = Kittens::findOne($id);
    if ($model === null) {
      throw new \yii\web\NotFoundHttpException("Kitten not found with ID: $id");
    }

    if ($model->delete()) {
      return 'Kitten deleted successfully';
    } else {
      return 'Failed to delete kitten';
    }
  }
}
