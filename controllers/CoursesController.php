<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use app\models\Courses;

class CoursesController extends ActiveController
{
  public $modelClass = 'app\models\Courses';

  public function actionIndex()
  {
    return Courses::find()->all();
  }

  public function actionView($id)
  {
    return Courses::findOne($id);
  }

  public function actionCreate()
  {
    $model = new Courses();
    $model->load(Yii::$app->getRequest()->getBodyParams(), '');
    if ($model->save()) {
      return $model;
    } else {
      return $model->errors;
    }
  }

  public function actionUpdate($id)
  {
    $model = Courses::findOne($id);
    if ($model === null) {
      throw new \yii\web\NotFoundHttpException("Course not found with ID: $id");
    }

    $model->load(Yii::$app->getRequest()->getBodyParams(), '');
    if ($model->save()) {
      return $model;
    } else {
      return $model->errors;
    }
  }

  public function actionDelete($id)
  {
    $model = Courses::findOne($id);
    if ($model === null) {
      throw new \yii\web\NotFoundHttpException("Course not found with ID: $id");
    }

    if ($model->delete()) {
      return 'Course deleted successfully';
    } else {
      return 'Failed to delete course';
    }
  }
}
