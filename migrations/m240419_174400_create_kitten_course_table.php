<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%kitten_course}}`.
 */
class m240419_174400_create_kitten_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('kitten_course', [
        'kitten_id' => $this->integer()->notNull(),
        'course_id' => $this->integer()->notNull(),
        'PRIMARY KEY(kitten_id, course_id)',
      ]);
      $this->addForeignKey(
        'fk-kitten_course-kitten_id',
        'kitten_course',
        'kitten_id',
        'kittens',
        'id',
        'CASCADE'
      );
      $this->addForeignKey(
        'fk-kitten_course-course_id',
        'kitten_course',
        'course_id',
        'courses',
        'id',
        'CASCADE'
      );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%kitten_course}}');
    }
}
