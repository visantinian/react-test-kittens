<?php

use yii\db\Migration;

/**
 * Class m240419_180000_insert_default_data
 */
class m240419_174455_create_default_data extends Migration
{
  /**
   * {@inheritdoc}
   */
  public function safeUp()
  {
    $this->batchInsert('courses', ['name', 'price'], [
      ['Курс "Тигидик"', 600],
      ['Курс "Я не їв вічність"', 350],
      ['Курс "Хопа"', 1000],
      ['Курс "Кусь"', 50],
      ['Курс "У всьому винен пес"', 600],
    ]);

    $this->batchInsert('kittens', ['name', 'last_name', 'age', 'phone_number'], [
      ['Мурчик', 'Муркевич', '2 роки', '+380345678907'],
      ['Бусинка', 'Булочкина', '4 місяці', '+380345678907'],
    ]);

    $kitten1Id = (new \yii\db\Query())->select('id')->from('kittens')->where(['name' => 'Мурчик', 'last_name' => 'Муркевич'])->scalar();
    $kitten2Id = (new \yii\db\Query())->select('id')->from('kittens')->where(['name' => 'Бусинка', 'last_name' => 'Булочкина'])->scalar();

    if ($kitten1Id && $kitten2Id) {
      $course1Id = (new \yii\db\Query())->select('id')->from('courses')->where(['name' => 'Курс "Тигидик"'])->scalar();
      $course5Id = (new \yii\db\Query())->select('id')->from('courses')->where(['name' => 'Курс "У всьому винен пес"'])->scalar();
      $course3Id = (new \yii\db\Query())->select('id')->from('courses')->where(['name' => 'Курс "Хопа"'])->scalar();
      $course4Id = (new \yii\db\Query())->select('id')->from('courses')->where(['name' => 'Курс "Кусь"'])->scalar();

      $this->batchInsert('kitten_course', ['kitten_id', 'course_id'], [
        [$kitten1Id, $course1Id],
        [$kitten1Id, $course5Id],
        [$kitten2Id, $course3Id],
        [$kitten2Id, $course4Id],
      ]);
    } else {
      echo "Failed to fetch kitten IDs, check data insertion.\n";
    }
  }


  /**
   * {@inheritdoc}
   */
  public function safeDown()
  {
    return false;
  }
}
