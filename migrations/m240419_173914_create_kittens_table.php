<?php

use yii\db\Migration;

class m240419_173914_create_kittens_table extends Migration
{
  public function safeUp()
  {
    $this->createTable('{{%kittens}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string()->notNull(),
      'last_name' => $this->string()->notNull(),
      'age' => $this->string()->notNull(),
      'phone_number' => $this->string()->notNull(),
    ]);
  }

  public function safeDown()
  {
    $this->dropTable('{{%kittens}}');
  }
}
