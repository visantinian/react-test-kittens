<?php

use yii\db\Migration;

class m240419_173809_create_courses_table extends Migration
{
  public function safeUp()
  {
    $this->createTable('{{%courses}}', [
      'id' => $this->primaryKey(),
      'name' => $this->string()->notNull(),
      'price' => $this->decimal(10, 2)->notNull(),
    ]);
  }

  public function safeDown()
  {
    $this->dropTable('{{%courses}}');
  }
}
