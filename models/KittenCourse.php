<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "kitten_course".
 *
 * @property int $kitten_id
 * @property int $course_id
 *
 * @property Courses $course
 * @property Kittens $kitten
 */
class KittenCourse extends ActiveRecord
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'kitten_course';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['kitten_id', 'course_id'], 'required'],
      [['kitten_id', 'course_id'], 'integer'],
      [['kitten_id', 'course_id'], 'unique', 'targetAttribute' => ['kitten_id', 'course_id']],
      [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Courses::className(), 'targetAttribute' => ['course_id' => 'id']],
      [['kitten_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kittens::className(), 'targetAttribute' => ['kitten_id' => 'id']],
    ];
  }

  /**
   * Gets query for [[Course]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getCourse()
  {
    return $this->hasOne(Courses::className(), ['id' => 'course_id']);
  }

  /**
   * Gets query for [[Kitten]].
   *
   * @return \yii\db\ActiveQuery
   */
  public function getKitten()
  {
    return $this->hasOne(Kittens::className(), ['id' => 'kitten_id']);
  }
}
