<?php

namespace app\models;

use yii\db\ActiveRecord;

class Courses extends ActiveRecord
{
  public static function tableName()
  {
    return 'courses'; // Assuming 'courses' is the name of your database table for courses
  }

  public function getKittens() {
    return $this->hasMany(Kittens::className(), ['id' => 'kitten_id'])
      ->viaTable('kitten_course', ['course_id' => 'id']);
  }


  // Define rules for validation
  public function rules()
  {
    return [
      [['name', 'price'], 'required'],
      [['price'], 'number'],
      [['name'], 'string', 'max' => 255],
    ];
  }
}
