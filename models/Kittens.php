<?php

namespace app\models;

use yii\db\ActiveRecord;

class Kittens extends ActiveRecord
{
  public static function tableName()
  {
    return 'kittens'; // Assuming 'kittens' is the name of your database table for kittens
  }

  public function getCourses() {
    return $this->hasMany(Courses::className(), ['id' => 'course_id'])
      ->viaTable('kitten_course', ['kitten_id' => 'id']);
  }

  // Define rules for validation
  public function rules()
  {
    return [
      [['name', 'last_name', 'age', 'phone_number'], 'required'],
      [['name', 'last_name'], 'string', 'max' => 255],
      [['phone_number', 'age'], 'string'],
    ];
  }
}
